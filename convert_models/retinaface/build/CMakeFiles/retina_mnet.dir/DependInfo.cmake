# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/ws/fr_recognition_ds/tensorrtx/retinaface/calibrator.cpp" "/ws/fr_recognition_ds/tensorrtx/retinaface/build/CMakeFiles/retina_mnet.dir/calibrator.cpp.o"
  "/ws/fr_recognition_ds/tensorrtx/retinaface/retina_mnet.cpp" "/ws/fr_recognition_ds/tensorrtx/retinaface/build/CMakeFiles/retina_mnet.dir/retina_mnet.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/cuda/include"
  "/usr/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/ws/fr_recognition_ds/tensorrtx/retinaface/build/CMakeFiles/decodeplugin.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
